CXX = g++
CXXFLAGS = -O3 -std=c++98 -Wall -Wno-narrowing -I src

PPENGINE_COMMONDEPS = src/ppengine.o src/engine.o src/sub/framehandlerdispatch.o src/log.o src/lib/hud.o src/sub/framehandler/unimplemented.o src/sub/framehandler/316.o src/sub/framehandler/3a8.o src/sub/framehandler/3d0.o src/sub/framehandler/42a.o src/sub/framehandler/456.o src/sub/framehandler/46c.o src/sub/framehandler/4d6.o src/sub/framehandler/4ec.o src/sub/framehandler/602.o src/sub/framehandler/66a.o src/sub/framehandler/72a.o src/sub/framehandler/808.o src/sub/framehandler/852.o src/sub/framehandler/8cc.o src/sub/framehandler/9e0.o src/sub/framehandler/a2e.o src/sub/framehandler/aba.o src/sub/framehandler/b2c.o src/sub/framehandler/bd0.o src/sub/framehandler/c34.o src/sub/framehandler/c68.o src/sub/framehandler/c86.o src/sub/common/6a8.o src/sub/common/1ad2.o src/sub/common/1ad8.o src/sub/common/72a.o src/sub/common/7b6.o src/sub/common/a2e.o src/sub/common/aba.o src/sub/common/1572.o src/sub/common/bc6.o src/sub/common/1ff0.o src/sub/common/1406.o src/sub/common/187e.o src/sub/common/18f4.o src/sub/common/353a.o src/sub/common/148e.o src/sub/common/1906.o src/sub/common/1844.o src/sub/common/1b2e.o src/sub/common/165a.o src/sub/common/16be.o src/sub/common/1b92.o src/sub/common/1786.o src/sub/common/1ba4.o src/sub/common/840.o src/sub/common/1928.o src/sub/common/199e.o src/sub/common/1708.o src/sub/common/a6a.o src/sub/common/1310.o src/sub/common/bc8.o src/sub/common/1a0a.o src/sub/common/1a26.o src/sub/common/17da.o src/sub/common/12a2.o src/sub/common/1c48.o src/sub/common/18dc.o src/sub/common/18c4.o src/sub/lib/bcd.o src/sub/data/race_seconds_lookup.o src/sub/data/track_curve_data.o src/sub/data/acceleration_lookup.o src/sub/data/loc_223e_lookup.o src/sub/data/required_qualifying_times.o src/sub/data/qualifying_bonus_lookup.o src/sub2/framehandler.o src/sub2/functionqueue.o src/sub2/queuedfunctions/1e6.o src/sub2/queuedfunctions/40c.o src/sub2/queuedfunctions/476.o src/sub2/queuedfunctions/554.o src/sub2/queuedfunctions/59e.o src/sub2/queuedfunctions/24d8.o src/sub2/perframefunctions/playercarmovement.o src/sub2/perframefunctions/cpucarmovement.o src/sub2/perframefunctions/cpucarspawning.o src/sub2/perframefunctions/cpucarai.o src/sub2/perframefunctions/cpucarhiding.o src/sub2/perframefunctions/carcollisions.o src/sub2/perframefunctions/scenerycollisions.o src/sub2/perframefunctions/puddlecollisions.o src/sub2/perframefunctions/carexplosions.o src/sub2/perframefunctions/common/random.o src/sub2/data/sub_1886_lookup.o

PPENGINE_TESTDEPS = src/sub/common/tests/1844.o src/sub/common/tests/148e.o src/sub/common/tests/2058.o src/sub/common/tests/212c.o src/sub/common/tests/235e.o src/sub/common/tests/2174.o src/sub/common/tests/21b4.o src/sub/common/tests/221a.o src/sub/common/tests/227c.o src/sub/common/tests/231e.o src/sub/common/tests/24b0.o src/sub/common/tests/244c.o src/sub2/perframefunctions/tests/cpucarhiding.o src/sub2/perframefunctions/tests/cpucarai.o src/sub2/perframefunctions/tests/cpucarmovement.o src/sub2/perframefunctions/tests/playercarmovement.o src/sub2/perframefunctions/tests/common/dump_moving_objects_failures.o

default: bin/ppengine.a

clean:
	rm `find src/ -name '*.o'` -rf

bin/ppengine.a: $(PPENGINE_COMMONDEPS)
	ar rvs bin/ppengine.a $(PPENGINE_COMMONDEPS) 

bin/tests: src/tests.c src/tests.h $(PPENGINE_COMMONDEPS) $(PPENGINE_TESTDEPS)
	$(CXX) $(CXXFLAGS) -o bin/tests src/tests.c $(PPENGINE_COMMONDEPS) $(PPENGINE_TESTDEPS)
 
src/ppengine.o: src/ppengine.c src/engine.h src/engine.h src/structs.h
	$(CXX) $(CXXFLAGS) -c src/ppengine.c -o src/ppengine.o

src/engine.o: src/engine.c src/engine.h src/sub/framehandlerdispatch.h src/sub2/functionqueue.h src/sub2/framehandler.h
	$(CXX) $(CXXFLAGS) -c src/engine.c -o src/engine.o

src/log.o: src/log.c src/log.h
	$(CXX) $(CXXFLAGS) -c src/log.c -o src/log.o

src/lib/hud.o: src/lib/hud.c src/lib/hud.h
	$(CXX) $(CXXFLAGS) -c src/lib/hud.c -o src/lib/hud.o

src/sub/framehandlerdispatch.o: src/sub/framehandlerdispatch.c src/sub/framehandlerdispatch.h src/log.h src/sub/framehandler/unimplemented.h src/sub/framehandler/316.h src/sub/framehandler/3a8.h src/sub/framehandler/3d0.h src/sub/framehandler/42a.h src/sub/framehandler/456.h src/sub/framehandler/46c.h src/sub/framehandler/4d6.h src/sub/framehandler/4ec.h src/sub/framehandler/602.h src/sub/framehandler/66a.h src/sub/framehandler/72a.h src/sub/framehandler/808.h src/sub/framehandler/852.h src/sub/framehandler/8cc.h src/sub/framehandler/9e0.h src/sub/framehandler/a2e.h src/sub/framehandler/aba.h src/sub/framehandler/b2c.h src/sub/framehandler/bd0.h src/sub/framehandler/c34.h src/sub/framehandler/c68.h src/sub/framehandler/c86.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandlerdispatch.c -o src/sub/framehandlerdispatch.o

src/sub/framehandler/unimplemented.o: src/sub/framehandler/unimplemented.c src/sub/framehandler/unimplemented.h src/log.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/unimplemented.c -o src/sub/framehandler/unimplemented.o

src/sub/framehandler/316.o: src/sub/framehandler/316.c src/sub/framehandler/316.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/316.c -o src/sub/framehandler/316.o

src/sub/framehandler/3a8.o: src/sub/framehandler/3a8.c src/sub/framehandler/3a8.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/3a8.c -o src/sub/framehandler/3a8.o

src/sub/framehandler/3d0.o: src/sub/framehandler/3d0.c src/sub/framehandler/3d0.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/3d0.c -o src/sub/framehandler/3d0.o

src/sub/framehandler/42a.o: src/sub/framehandler/42a.c src/sub/framehandler/42a.h src/sub/common/6a8.h src/engine.h src/sub/detectinsertedcoin.h src/sub2/functionqueue.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/42a.c -o src/sub/framehandler/42a.o

src/sub/framehandler/456.o: src/sub/framehandler/456.c src/sub/framehandler/456.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/456.c -o src/sub/framehandler/456.o

src/sub/framehandler/46c.o: src/sub/framehandler/46c.c src/sub/framehandler/46c.h src/sub/common/a2e.h src/engine.h src/sub2/functionqueue.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/46c.c -o src/sub/framehandler/46c.o

src/sub/framehandler/4d6.o: src/sub/framehandler/4d6.c src/sub/framehandler/4d6.h src/sub/common/aba.h src/sub/common/1c48.h src/engine.h src/sub/detectinsertedcoin.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/4d6.c -o src/sub/framehandler/4d6.o

src/sub/framehandler/4ec.o: src/sub/framehandler/4ec.c src/sub/framehandler/4ec.h src/sub/common/148e.h src/sub/common/1572.h src/sub/common/1ff0.h src/sub/common/1906.h src/sub/common/1844.h src/sub/common/1b2e.h src/sub/common/165a.h src/sub/common/16be.h src/sub/detectinsertedcoin.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/4ec.c -o src/sub/framehandler/4ec.o

src/sub/framehandler/602.o: src/sub/framehandler/602.c src/sub/framehandler/602.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/602.c -o src/sub/framehandler/602.o

src/sub/framehandler/66a.o: src/sub/framehandler/66a.c src/sub/framehandler/66a.h src/sub/common/6a8.h src/sub2/functionqueue.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/66a.c -o src/sub/framehandler/66a.o

src/sub/framehandler/72a.o: src/sub/framehandler/72a.c src/sub/framehandler/72a.h src/sub2/functionqueue.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/72a.c -o src/sub/framehandler/72a.o

src/sub/framehandler/808.o: src/sub/framehandler/808.c src/sub/framehandler/808.h src/sub/common/bc6.h src/sub/common/840.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/808.c -o src/sub/framehandler/808.o

src/sub/framehandler/852.o: src/sub/framehandler/852.c src/sub/framehandler/852.h src/sub/common/1572.h src/sub/common/bc6.h src/sub/common/1ff0.h src/sub2/functionqueue.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/852.c -o src/sub/framehandler/852.o

src/sub/framehandler/8cc.o: src/sub/framehandler/8cc.c src/sub/framehandler/8cc.h src/sub/common/148e.h src/sub/common/1572.h src/sub/common/1ff0.h src/sub/common/1906.h src/sub/common/1928.h src/sub/common/bc6.h src/sub/common/1b2e.h src/sub/common/165a.h src/sub/common/16be.h src/sub2/functionqueue.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/8cc.c -o src/sub/framehandler/8cc.o

src/sub/framehandler/9e0.o: src/sub/framehandler/9e0.c src/sub/framehandler/9e0.h src/sub/common/bc6.h src/sub/lib/bcd.h src/lib/hud.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/9e0.c -o src/sub/framehandler/9e0.o

src/sub/framehandler/a2e.o: src/sub/framehandler/a2e.c src/sub/framehandler/a2e.h src/sub/common/a2e.h src/sub/common/840.h src/sub/common/1ad2.h src/sub/common/a6a.h src/sub2/functionqueue.h src/sub/data/race_seconds_lookup.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/a2e.c -o src/sub/framehandler/a2e.o

src/sub/framehandler/b2c.o: src/sub/framehandler/b2c.c src/sub/framehandler/b2c.h src/sub/common/148e.h src/sub/common/1572.h src/sub/common/1ff0.h src/sub/common/1906.h src/sub/common/1928.h src/sub/common/199e.h src/sub/common/bc6.h src/sub/common/1b2e.h src/sub/common/165a.h src/sub/common/16be.h src/lib/hud.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/b2c.c -o src/sub/framehandler/b2c.o

src/sub/framehandler/aba.o: src/sub/framehandler/aba.c src/sub/framehandler/aba.h src/sub/common/aba.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/aba.c -o src/sub/framehandler/aba.o

src/sub/framehandler/bd0.o: src/sub/framehandler/bd0.c src/sub/framehandler/bd0.h src/sub/common/1ff0.h src/sub/common/1906.h src/sub/common/1928.h src/sub/common/199e.h src/sub/common/bc6.h src/sub/common/165a.h src/sub/common/16be.h src/sub/common/1310.h src/sub/common/1b92.h src/sub/common/1786.h src/sub/common/1ba4.h src/sub2/functionqueue.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/bd0.c -o src/sub/framehandler/bd0.o

src/sub/framehandler/c34.o: src/sub/framehandler/c34.c src/sub/framehandler/c34.h src/sub/common/bc8.h src/sub/common/1310.h src/lib/hud.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/c34.c -o src/sub/framehandler/c34.o

src/sub/framehandler/c68.o: src/sub/framehandler/c68.c src/sub/framehandler/c68.h src/sub/common/bc8.h src/sub/common/12a2.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/c68.c -o src/sub/framehandler/c68.o

src/sub/framehandler/c86.o: src/sub/framehandler/c86.c src/sub/framehandler/c86.h src/sub/common/bc8.h src/sub/common/18dc.h src/sub/common/12a2.h src/sub/common/18f4.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/framehandler/c86.c -o src/sub/framehandler/c86.o

src/sub/common/6a8.o: src/sub/common/6a8.c src/sub/common/6a8.h src/sub/common/1ad2.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/6a8.c -o src/sub/common/6a8.o

src/sub/common/1ad2.o: src/sub/common/1ad2.c src/sub/common/1ad2.h src/sub/common/1ad8.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1ad2.c -o src/sub/common/1ad2.o

src/sub/common/1ad8.o: src/sub/common/1ad8.c src/sub/common/1ad8.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1ad8.c -o src/sub/common/1ad8.o

src/sub/common/72a.o: src/sub/common/72a.c src/sub/common/72a.h src/sub/common/7b6.h src/sub/common/a6a.h src/engine.h src/sub2/functionqueue.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/72a.c -o src/sub/common/72a.o

src/sub/common/7b6.o: src/sub/common/7b6.c src/sub/common/7b6.h src/sub/common/7b6.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/7b6.c -o src/sub/common/7b6.o

src/sub/common/a2e.o: src/sub/common/a2e.c src/sub/common/a2e.h src/sub/common/1ad2.h src/sub/common/840.h src/sub/data/race_seconds_lookup.h src/engine.h src/sub2/functionqueue.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/a2e.c -o src/sub/common/a2e.o

src/sub/common/aba.o: src/sub/common/aba.c src/sub/common/aba.h src/sub/common/1572.h src/sub/common/bc6.h src/sub/common/1ff0.h src/sub/common/1406.h src/engine.h src/sub2/functionqueue.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/aba.c -o src/sub/common/aba.o

src/sub/common/1572.o: src/sub/common/1572.c src/sub/common/1572.h src/sub/data/track_curve_data.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1572.c -o src/sub/common/1572.o

src/sub/common/bc6.o: src/sub/common/bc6.c src/sub/common/bc6.h src/sub/common/187e.h src/sub/common/bc8.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/bc6.c -o src/sub/common/bc6.o

src/sub/common/1ff0.o: src/sub/common/1ff0.c src/sub/common/1ff0.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1ff0.c -o src/sub/common/1ff0.o

src/sub/common/1406.o: src/sub/common/1406.c src/sub/common/1406.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1406.c -o src/sub/common/1406.o

src/sub/common/187e.o: src/sub/common/187e.c src/sub/common/187e.h src/sub/common/18f4.h src/sub/common/353a.h src/sub/common/1c48.h src/sub/common/18c4.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/187e.c -o src/sub/common/187e.o

src/sub/common/18f4.o: src/sub/common/18f4.c src/sub/common/18f4.h src/sub/lib/bcd.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/18f4.c -o src/sub/common/18f4.o

src/sub/common/353a.o: src/sub/common/353a.c src/sub/common/353a.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/353a.c -o src/sub/common/353a.o

src/sub/common/148e.o: src/sub/common/148e.c src/sub/common/148e.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/148e.c -o src/sub/common/148e.o

src/sub/common/1906.o: src/sub/common/1906.c src/sub/common/1906.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1906.c -o src/sub/common/1906.o

src/sub/common/1844.o: src/sub/common/1844.c src/sub/common/1844.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1844.c -o src/sub/common/1844.o

src/sub/common/1b2e.o: src/sub/common/1b2e.c src/sub/common/1b2e.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1b2e.c -o src/sub/common/1b2e.o

src/sub/common/165a.o: src/sub/common/165a.c src/sub/common/165a.h src/sub/data/required_qualifying_times.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/165a.c -o src/sub/common/165a.o

src/sub/common/16be.o: src/sub/common/16be.c src/sub/common/16be.h src/sub/common/1ad8.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/16be.c -o src/sub/common/16be.o

src/sub/common/1b92.o: src/sub/common/1b92.c src/sub/common/1b92.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1b92.c -o src/sub/common/1b92.o

src/sub/common/1786.o: src/sub/common/1786.c src/sub/common/1786.h src/engine.h src/sub2/functionqueue.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1786.c -o src/sub/common/1786.o

src/sub/common/1ba4.o: src/sub/common/1ba4.c src/sub/common/1ba4.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1ba4.c -o src/sub/common/1ba4.o

src/sub/common/840.o: src/sub/common/840.c src/sub/common/840.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/840.c -o src/sub/common/840.o

src/sub/common/1928.o: src/sub/common/1928.c src/sub/common/1928.h src/sub/common/1ad2.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1928.c -o src/sub/common/1928.o

src/sub/common/199e.o: src/sub/common/199e.c src/sub/common/199e.h src/sub/common/353a.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/199e.c -o src/sub/common/199e.o

src/sub/common/1708.o: src/sub/common/1708.c src/sub/common/1708.h src/sub/data/race_seconds_lookup.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1708.c -o src/sub/common/1708.o

src/sub/common/a6a.o: src/sub/common/a6a.c src/sub/common/a6a.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/a6a.c -o src/sub/common/a6a.o

src/sub/common/1310.o: src/sub/common/1310.c src/sub/common/1310.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1310.c -o src/sub/common/1310.o

src/sub/common/bc8.o: src/sub/common/bc8.c src/sub/common/bc8.h src/sub/common/1a0a.h src/sub/common/1a26.h src/sub/common/17da.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/bc8.c -o src/sub/common/bc8.o

src/sub/common/1a0a.o: src/sub/common/1a0a.c src/sub/common/1a0a.h src/engine.h src/lib/hud.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1a0a.c -o src/sub/common/1a0a.o

src/sub/common/1a26.o: src/sub/common/1a26.c src/sub/common/1a26.h src/lib/hud.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1a26.c -o src/sub/common/1a26.o

src/sub/common/17da.o: src/sub/common/17da.c src/sub/common/17da.h src/sub/common/1844.h src/sub/lib/bcd.h src/lib/hud.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/17da.c -o src/sub/common/17da.o

src/sub/common/12a2.o: src/sub/common/12a2.c src/sub/common/12a2.h src/lib/hud.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/12a2.c -o src/sub/common/12a2.o

src/sub/common/1c48.o: src/sub/common/1c48.c src/sub/common/1c48.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/1c48.c -o src/sub/common/1c48.o

src/sub/common/18dc.o: src/sub/common/18dc.c src/sub/common/18dc.h src/sub/common/1c48.h src/sub/common/18c4.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/18dc.c -o src/sub/common/18dc.o

src/sub/common/18c4.o: src/sub/common/18c4.c src/sub/common/18c4.h src/sub/common/1c48.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/18c4.c -o src/sub/common/18c4.o

src/sub/lib/bcd.o: src/sub/lib/bcd.c src/sub/lib/bcd.h
	$(CXX) $(CXXFLAGS) -c src/sub/lib/bcd.c -o src/sub/lib/bcd.o

src/sub/data/race_seconds_lookup.o: src/sub/data/race_seconds_lookup.c src/sub/data/race_seconds_lookup.h
	$(CXX) $(CXXFLAGS) -c src/sub/data/race_seconds_lookup.c -o src/sub/data/race_seconds_lookup.o

src/sub/data/track_curve_data.o: src/sub/data/track_curve_data.c src/sub/data/track_curve_data.h
	$(CXX) $(CXXFLAGS) -c src/sub/data/track_curve_data.c -o src/sub/data/track_curve_data.o

src/sub/data/acceleration_lookup.o: src/sub/data/acceleration_lookup.c src/sub/data/acceleration_lookup.h
	$(CXX) $(CXXFLAGS) -c src/sub/data/acceleration_lookup.c -o src/sub/data/acceleration_lookup.o

src/sub/data/loc_223e_lookup.o: src/sub/data/loc_223e_lookup.c src/sub/data/loc_223e_lookup.h
	$(CXX) $(CXXFLAGS) -c src/sub/data/loc_223e_lookup.c -o src/sub/data/loc_223e_lookup.o

src/sub/data/required_qualifying_times.o: src/sub/data/required_qualifying_times.c src/sub/data/required_qualifying_times.h
	$(CXX) $(CXXFLAGS) -c src/sub/data/required_qualifying_times.c -o src/sub/data/required_qualifying_times.o

src/sub/data/qualifying_bonus_lookup.o: src/sub/data/qualifying_bonus_lookup.c src/sub/data/qualifying_bonus_lookup.h
	$(CXX) $(CXXFLAGS) -c src/sub/data/qualifying_bonus_lookup.c -o src/sub/data/qualifying_bonus_lookup.o

src/sub2/framehandler.o: src/sub2/framehandler.c src/sub2/framehandler.h src/sub2/perframefunctions/playercarmovement.h src/sub2/perframefunctions/cpucarmovement.h src/sub2/perframefunctions/cpucarspawning.h src/sub2/perframefunctions/cpucarai.h src/sub2/perframefunctions/cpucarhiding.h src/sub2/perframefunctions/carcollisions.h src/sub2/perframefunctions/scenerycollisions.h src/sub2/perframefunctions/puddlecollisions.h src/sub2/perframefunctions/carexplosions.h
	$(CXX) $(CXXFLAGS) -c src/sub2/framehandler.c -o src/sub2/framehandler.o 

src/sub2/functionqueue.o: src/sub2/functionqueue.c src/sub2/functionqueue.h src/sub2/queuedfunctions/1e6.h src/sub2/queuedfunctions/40c.h src/sub2/queuedfunctions/476.h src/sub2/queuedfunctions/554.h src/sub2/queuedfunctions/59e.h src/sub2/queuedfunctions/24d8.h
	$(CXX) $(CXXFLAGS) -c src/sub2/functionqueue.c -o src/sub2/functionqueue.o 

src/sub2/queuedfunctions/1e6.o: src/sub2/queuedfunctions/1e6.c src/sub2/queuedfunctions/1e6.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/queuedfunctions/1e6.c -o src/sub2/queuedfunctions/1e6.o 

src/sub2/queuedfunctions/40c.o: src/sub2/queuedfunctions/40c.c src/sub2/queuedfunctions/40c.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/queuedfunctions/40c.c -o src/sub2/queuedfunctions/40c.o 

src/sub2/queuedfunctions/476.o: src/sub2/queuedfunctions/476.c src/sub2/queuedfunctions/476.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/queuedfunctions/476.c -o src/sub2/queuedfunctions/476.o 

src/sub2/queuedfunctions/554.o: src/sub2/queuedfunctions/554.c src/sub2/queuedfunctions/554.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/queuedfunctions/554.c -o src/sub2/queuedfunctions/554.o 

src/sub2/queuedfunctions/59e.o: src/sub2/queuedfunctions/59e.c src/sub2/queuedfunctions/59e.h src/sub2/queuedfunctions/1e6.h src/sub2/queuedfunctions/24d8.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/queuedfunctions/59e.c -o src/sub2/queuedfunctions/59e.o 

src/sub2/queuedfunctions/24d8.o: src/sub2/queuedfunctions/24d8.c src/sub2/queuedfunctions/24d8.h
	$(CXX) $(CXXFLAGS) -c src/sub2/queuedfunctions/24d8.c -o src/sub2/queuedfunctions/24d8.o 

src/sub2/perframefunctions/playercarmovement.o: src/sub2/perframefunctions/playercarmovement.c src/sub2/perframefunctions/playercarmovement.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/playercarmovement.c -o src/sub2/perframefunctions/playercarmovement.o 

src/sub2/perframefunctions/cpucarmovement.o: src/sub2/perframefunctions/cpucarmovement.c src/sub2/perframefunctions/cpucarmovement.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/cpucarmovement.c -o src/sub2/perframefunctions/cpucarmovement.o 

src/sub2/perframefunctions/cpucarspawning.o: src/sub2/perframefunctions/cpucarspawning.c src/sub2/perframefunctions/cpucarspawning.h src/sub2/perframefunctions/common/random.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/cpucarspawning.c -o src/sub2/perframefunctions/cpucarspawning.o 

src/sub2/perframefunctions/cpucarai.o: src/sub2/perframefunctions/cpucarai.c src/sub2/perframefunctions/cpucarai.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/cpucarai.c -o src/sub2/perframefunctions/cpucarai.o 

src/sub2/perframefunctions/cpucarhiding.o: src/sub2/perframefunctions/cpucarhiding.c src/sub2/perframefunctions/cpucarhiding.h src/sub2/perframefunctions/common/random.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/cpucarhiding.c -o src/sub2/perframefunctions/cpucarhiding.o

src/sub2/perframefunctions/carcollisions.o: src/sub2/perframefunctions/carcollisions.c src/sub2/perframefunctions/carcollisions.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/carcollisions.c -o src/sub2/perframefunctions/carcollisions.o

src/sub2/perframefunctions/scenerycollisions.o: src/sub2/perframefunctions/scenerycollisions.c src/sub2/perframefunctions/scenerycollisions.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/scenerycollisions.c -o src/sub2/perframefunctions/scenerycollisions.o

src/sub2/perframefunctions/puddlecollisions.o: src/sub2/perframefunctions/puddlecollisions.c src/sub2/perframefunctions/puddlecollisions.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/puddlecollisions.c -o src/sub2/perframefunctions/puddlecollisions.o

src/sub2/perframefunctions/carexplosions.o: src/sub2/perframefunctions/carexplosions.c src/sub2/perframefunctions/carexplosions.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/carexplosions.c -o src/sub2/perframefunctions/carexplosions.o

src/sub2/perframefunctions/common/random.o: src/sub2/perframefunctions/common/random.c src/sub2/perframefunctions/common/random.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/common/random.c -o src/sub2/perframefunctions/common/random.o 

src/sub2/data/sub_1886_lookup.o: src/sub2/data/sub_1886_lookup.c src/sub2/data/sub_1886_lookup.h
	$(CXX) $(CXXFLAGS) -c src/sub2/data/sub_1886_lookup.c -o src/sub2/data/sub_1886_lookup.o 

src/sub/common/tests/1844.o: src/sub/common/tests/1844.c src/sub/common/tests/1844.h src/sub/common/1844.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/tests/1844.c -o src/sub/common/tests/1844.o

src/sub/common/tests/148e.o: src/sub/common/tests/148e.c src/sub/common/tests/148e.h src/sub/common/148e.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/tests/148e.c -o src/sub/common/tests/148e.o

src/sub/common/tests/2058.o: src/sub/common/tests/2058.c src/sub/common/tests/2058.h src/sub/common/1ff0.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/tests/2058.c -o src/sub/common/tests/2058.o

src/sub/common/tests/212c.o: src/sub/common/tests/212c.c src/sub/common/tests/212c.h src/sub/common/1ff0.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/tests/212c.c -o src/sub/common/tests/212c.o

src/sub/common/tests/235e.o: src/sub/common/tests/235e.c src/sub/common/tests/235e.h src/sub/common/1ff0.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/tests/235e.c -o src/sub/common/tests/235e.o

src/sub/common/tests/2174.o: src/sub/common/tests/2174.c src/sub/common/tests/2174.h src/sub/common/1ff0.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/tests/2174.c -o src/sub/common/tests/2174.o

src/sub/common/tests/21b4.o: src/sub/common/tests/21b4.c src/sub/common/tests/21b4.h src/sub/common/1ff0.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/tests/21b4.c -o src/sub/common/tests/21b4.o

src/sub/common/tests/221a.o: src/sub/common/tests/221a.c src/sub/common/tests/221a.h src/sub/common/1ff0.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/tests/221a.c -o src/sub/common/tests/221a.o

src/sub/common/tests/227c.o: src/sub/common/tests/227c.c src/sub/common/tests/227c.h src/sub/common/1ff0.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/tests/227c.c -o src/sub/common/tests/227c.o

src/sub/common/tests/231e.o: src/sub/common/tests/231e.c src/sub/common/tests/231e.h src/sub/common/1ff0.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/tests/231e.c -o src/sub/common/tests/231e.o

src/sub/common/tests/24b0.o: src/sub/common/tests/24b0.c src/sub/common/tests/24b0.h src/sub/common/1ff0.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/tests/24b0.c -o src/sub/common/tests/24b0.o

src/sub/common/tests/244c.o: src/sub/common/tests/244c.c src/sub/common/tests/244c.h src/sub/common/1ff0.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub/common/tests/244c.c -o src/sub/common/tests/244c.o

src/sub2/perframefunctions/tests/cpucarhiding.o: src/sub2/perframefunctions/tests/cpucarhiding.c src/sub2/perframefunctions/tests/cpucarhiding.h src/sub2/perframefunctions/cpucarhiding.h src/engine.h src/sub2/perframefunctions/tests/common/dump_moving_objects_failures.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/tests/cpucarhiding.c -o src/sub2/perframefunctions/tests/cpucarhiding.o

src/sub2/perframefunctions/tests/cpucarai.o: src/sub2/perframefunctions/tests/cpucarai.c src/sub2/perframefunctions/tests/cpucarai.h src/sub2/perframefunctions/cpucarai.h src/engine.h src/sub2/perframefunctions/tests/common/dump_moving_objects_failures.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/tests/cpucarai.c -o src/sub2/perframefunctions/tests/cpucarai.o

src/sub2/perframefunctions/tests/cpucarmovement.o: src/sub2/perframefunctions/tests/cpucarmovement.c src/sub2/perframefunctions/tests/cpucarmovement.h src/sub2/perframefunctions/cpucarmovement.h src/engine.h src/sub2/perframefunctions/tests/common/dump_moving_objects_failures.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/tests/cpucarmovement.c -o src/sub2/perframefunctions/tests/cpucarmovement.o

src/sub2/perframefunctions/tests/playercarmovement.o: src/sub2/perframefunctions/tests/playercarmovement.c src/sub2/perframefunctions/tests/playercarmovement.h src/sub2/perframefunctions/playercarmovement.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/tests/playercarmovement.c -o src/sub2/perframefunctions/tests/playercarmovement.o

src/sub2/perframefunctions/tests/common/dump_moving_objects_failures.o: src/sub2/perframefunctions/tests/common/dump_moving_objects_failures.c src/sub2/perframefunctions/tests/common/dump_moving_objects_failures.h src/engine.h
	$(CXX) $(CXXFLAGS) -c src/sub2/perframefunctions/tests/common/dump_moving_objects_failures.c -o src/sub2/perframefunctions/tests/common/dump_moving_objects_failures.o


