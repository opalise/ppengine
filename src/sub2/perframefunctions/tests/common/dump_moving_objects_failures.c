#include <stdio.h>
#include "engine.h"

void dump_moving_objects_failures(struct movingobject *actual_moving_objects,struct movingobject *expected_moving_objects,int num_objects) {
	int index;
	struct movingobject *current_actual;
	struct movingobject *current_expected;
	current_actual=actual_moving_objects;
	current_expected=expected_moving_objects;
	for (index=0; index<num_objects; index++) {
		if  (current_actual->type_and_visibility!=current_expected->type_and_visibility) {
			printf("- offset %d, type_and_visibility, expected %x, actual %x\n",index,current_expected->type_and_visibility,current_actual->type_and_visibility);
		}
		if  (current_actual->forward_position!=current_expected->forward_position) {
			printf("- offset %d, forward_position, expected %x, actual %x\n",index,current_expected->forward_position,current_actual->forward_position);
		}
		if  (current_actual->sideways_position!=current_expected->sideways_position) {
			printf("- offset %d, sideways_position, expected %x, actual %x\n",index,current_expected->sideways_position,current_actual->sideways_position);
		}
		if  (current_actual->player_forward_position_difference!=current_expected->player_forward_position_difference) {
			printf("- offset %d, player_forward_position_difference, expected %x, actual %x\n",index,current_expected->player_forward_position_difference,current_actual->player_forward_position_difference);
		}
		if  (current_actual->player_sideways_position_difference!=current_expected->player_sideways_position_difference) {
			printf("- offset %d, player_sideways_position_difference, expected %x, actual %x\n",index,current_expected->player_sideways_position_difference,current_actual->player_sideways_position_difference);
		}
		if  (current_actual->display_variant!=current_expected->display_variant) {
			printf("- offset %d, display_variant, expected %x, actual %x\n",index,current_expected->display_variant,current_actual->display_variant);
		}
		if  (current_actual->current_speed!=current_expected->current_speed) {
			printf("- offset %d, current_speed, expected %x, actual %x\n",index,current_expected->current_speed,current_actual->current_speed);
		}
		if  (current_actual->maximum_speed!=current_expected->maximum_speed) {
			printf("- offset %d, maximum_speed, expected %x, actual %x\n",index,current_expected->maximum_speed,current_actual->maximum_speed);
		}
		if  (current_actual->unknown_attribute!=current_expected->unknown_attribute) {
			printf("- offset %d, unknown_attribute, expected %x, actual %x\n",index,current_expected->unknown_attribute,current_actual->unknown_attribute);
		}
		if  (current_actual->sideways_movement!=current_expected->sideways_movement) {
			printf("- offset %d, sideways_movement, expected %x, actual %x\n",index,current_expected->sideways_movement,current_actual->sideways_movement);
		}
		if  (current_actual->one_time_speed_modification!=current_expected->one_time_speed_modification) {
			printf("- offset %d, one_time_speed_modification, expected %x, actual %x\n",index,current_expected->one_time_speed_modification,current_actual->one_time_speed_modification);
		}
		if  (current_actual->explosion_countdown!=current_expected->explosion_countdown) {
			printf("- offset %d, explosion_countdown, expected %x, actual %x\n",index,current_expected->explosion_countdown,current_actual->explosion_countdown);
		}
		current_actual++;
		current_expected++;
	}
}

