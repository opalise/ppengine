#include "sub/framehandler/c34.h"
#include "sub/common/bc8.h"
#include "sub/common/1310.h"
#include "lib/hud.h"
#include "engine.h"
#include <cstdio>

void frame_handler_c34() {
	common_bc8();
	common_1310();

	if (ticks_remaining>0) {
		// loc_c46
		ticks_remaining--;
		if (ticks_remaining==0) {
			jump_table_subfunction_number++;
			qualifying_session_state=0;
			time_bonus=seconds_remaining;
			printf("c34: setting time_bonus to %d\n",time_bonus);
			clear_hud_characters(709,24);
		}
	} else {
		ticks_remaining=30;
	}
}

