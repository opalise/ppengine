#include "sub/framehandler/4d6.h"
#include "sub/common/aba.h"
#include "sub/common/1c48.h"
#include "engine.h"
#include "sub/detectinsertedcoin.h"

void frame_handler_4d6() {
	DETECT_INSERTED_COIN
	common_1c48(38,high_score_bcd,0x2d);
	common_aba();
}

