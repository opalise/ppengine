#include <stdio.h>
#include <stdlib.h>
#include "log.h"

void frame_handler_unimplemented() {
	sprintf(log_get_buffer(),"%s: unable to continue execution - terminating",__func__);
	log_write_buffer_message();

	exit(EXIT_FAILURE);
}

