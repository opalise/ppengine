#include "sub/framehandler/808.h"
#include "sub/common/bc6.h"
#include "sub/common/840.h"
#include "engine.h"
#include "sub2/functionqueue.h"

void frame_handler_808() {
	common_bc6();
	if (ticks_remaining>0) {
		// loc_818
		ticks_remaining--;
		if (ticks_remaining==0) {
			jump_table_subfunction_number++;
			queue_sub2_function(3);
			common_840();
		}
	} else {
		ticks_remaining=0xb4;
	}
}

