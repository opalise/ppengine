#include "sub/framehandler/ca0.h"
#include "sub/common/bc8.h"
#include "sub/common/187e.h"
#include "sub/common/141c.h"
#include "sub/common/d32.h"
#include "engine.h"

// i suspect this is some kind of high score table initialisation
// try tidying up the previous frame handler and rewriting this
// one to just return to the title screen, see what happens

void frame_handler_ca0() {
	uint32_t common_34de_result;

	previous_lap_time_display_ticks_remaining=9;
	session_end_animation_ticks_remaining=1;
	common_bc8();
	common_187e();
	unknown_a880=4;
	unknown_a882=4;
	common_141c();
	current_score_outside_gameplay_bcd=current_score_times_100_bcd;
	unknown_80e8=0;
	engine_sound_pitch=9;
	unknown_802d=0;
	unknown_802f=0;
	unknown_807d=9;
	unknown_807f=9;

	common_34de_result=common_34de(current_score_times_100_bcd<<12);
	unknown_8025=common_34de_result&0xff;
	unknown_8027=(common_34de_result>>8)&0xff;
	if (unknown_82a6==0) {
		unknown_8029=0;
		unknown_802b=0;
	} else {
		common_34de_result=common_34de(race_time_elapsed_bcd&0xffff000f);
		unknown_8029=common_34de_result&0xf;
		unknown_802b=(common_34de_result>>8)&0xf;
	}
	unknown_8022=2;
	unknown_8020=0x73;
	common_d32();
	unknown_810e=0;
	jump_table_subfunction_number++;
}

uint32_t common_34de(uint32_t param) {
	// TODO: implement
}


