#include "sub/framehandler/42a.h"
#include "sub/detectinsertedcoin.h"
#include "engine.h"
#include "sub/common/6a8.h"
#include "sub2/functionqueue.h"

void frame_handler_42a() {
	DETECT_INSERTED_COIN
	waiting_for_coin=1;
	queue_sub2_function(0x5);
	common_6a8();
}

