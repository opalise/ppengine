#include <inttypes.h>
#include <cstdio>
#include "sub/common/18f4.h"
#include "sub/lib/bcd.h"

uint32_t common_18f4(uint32_t bcd_number_1, uint32_t bcd_number_2) {
	uint32_t result;
	uint32_t native_number_1;
	uint32_t native_number_2;
	uint32_t added_natives;

	native_number_1=bcd_to_native(bcd_number_1);
	native_number_2=bcd_to_native(bcd_number_2);
	added_natives=native_number_1+native_number_2;
	result=native_to_bcd(added_natives);
	return result;
}


