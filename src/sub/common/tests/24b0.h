#ifndef __SUB_COMMON_TESTS_24B0_H
#define __SUB_COMMON_TESTS_24B0_H

#include <inttypes.h>

struct common_24b0_test {
	uint16_t pre_full_race_distance_completed;
	uint16_t pre_waiting_for_coin;
	uint16_t pre_seconds_into_session;
	int16_t pre_clipped_steering_candidate_2;
	int16_t pre_high_precision_sideways_position;
	int16_t pre_steering_wheel_frame_delta;
	uint16_t pre_machine_uptime;
	int16_t pre_opponent_car_grinding_sideways_movement;
	uint16_t pre_qualifying_engine_started;
	int16_t pre_curve_at_player_forward_position;
	int16_t post_clipped_steering_candidate_2;
	int16_t post_opponent_car_grinding_sideways_movement;
};

extern int num_common_24b0_tests;
extern struct common_24b0_test common_24b0_tests[];

void run_common_24b0_tests();

#endif
