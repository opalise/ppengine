#ifndef __SUB_COMMON_TESTS_221A_H
#define __SUB_COMMON_TESTS_221A_H

#include <inttypes.h>

struct common_221a_test {
	int16_t pre_unknown_8240;
	uint16_t pre_internal_brake_pedal;
	int16_t pre_unknown_823a;
	uint16_t pre_qualifying_engine_started;
	uint16_t pre_race_initial_timer_switch_2;
	int16_t post_unknown_823e;	
};

extern int num_common_221a_tests;
extern struct common_221a_test common_221a_tests[];

void run_common_221a_tests();

#endif
