#ifndef __SUB_COMMON_TESTS_212C_H
#define __SUB_COMMON_TESTS_212C_H

#include <inttypes.h>

struct common_212c_test {
	int16_t pre_target_engine_revs;
	uint16_t pre_race_initial_timer_switch_2;
	uint16_t pre_internal_accel_pedal;
	int16_t post_target_revs_increase;
};

extern int num_common_212c_tests;
extern struct common_212c_test common_212c_tests[];

void run_common_212c_tests();

#endif
