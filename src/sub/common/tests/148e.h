#ifndef __SUB_COMMON_TESTS_148E_H
#define __SUB_COMMON_TESTS_148E_H

#include <inttypes.h>

struct common_148e_test {
	int16_t pre_car_slowdown_factor;
	int16_t pre_high_precision_speed;
	int32_t pre_current_speed_8_bit_candidate_3;
	int16_t post_car_slowdown_factor;
	int16_t post_high_precision_speed;	
	int32_t post_current_speed_8_bit_candidate_3;
};

extern int num_common_148e_tests;
extern struct common_148e_test common_148e_tests[];

void run_common_148e_tests();

#endif
