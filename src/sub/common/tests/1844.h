#ifndef __SUB_COMMON_TESTS_1844_H
#define __SUB_COMMON_TESTS_1844_H

#include <inttypes.h>

struct common_1844_test {
	int16_t high_precision_sideways_position;
	int16_t curve_at_player_forward_position;
	int32_t current_speed_8_bit_candidate_3;
	int32_t current_speed_8_bit;
};

extern int num_common_1844_tests;
extern struct common_1844_test common_1844_tests[];

void run_common_1844_tests();

#endif
