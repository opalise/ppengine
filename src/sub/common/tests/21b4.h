#ifndef __SUB_COMMON_TESTS_21B4_H
#define __SUB_COMMON_TESTS_21B4_H

#include <inttypes.h>

struct common_21b4_test {
	int16_t pre_target_actual_revs_relationship;
	int16_t pre_target_revs_increase;
	uint16_t pre_gear_hi_5_lo_a;
	int16_t pre_unknown_824c;
	int16_t pre_unknown_8236;
	int16_t pre_unknown_8238;
	uint16_t post_unknown_822c;
	int16_t post_unknown_823a;
};

extern int num_common_21b4_tests;
extern struct common_21b4_test common_21b4_tests[];

void run_common_21b4_tests();

#endif
