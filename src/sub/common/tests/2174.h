#ifndef __SUB_COMMON_TESTS_2174_H
#define __SUB_COMMON_TESTS_2174_H

#include <inttypes.h>

struct common_2174_test {
	int16_t pre_target_engine_revs;
	int16_t pre_actual_engine_revs;
	int16_t post_unknown_8236;
	int16_t post_unknown_8238;	
};

extern int num_common_2174_tests;
extern struct common_2174_test common_2174_tests[];

void run_common_2174_tests();

#endif
