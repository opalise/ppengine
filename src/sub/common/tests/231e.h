#ifndef __SUB_COMMON_TESTS_231E_H
#define __SUB_COMMON_TESTS_231E_H

#include <inttypes.h>

struct common_231e_test {
	int16_t pre_unknown_8240;
	int16_t pre_unknown_8248;
	int32_t pre_unknown_8268;
	int16_t pre_high_precision_speed;
	int16_t post_unknown_824c;
	int16_t post_change_to_speed;
	int16_t post_high_precision_speed;
};

extern int num_common_231e_tests;
extern struct common_231e_test common_231e_tests[];

void run_common_231e_tests();

#endif
