#ifndef __SUB_COMMON_TESTS_227C_H
#define __SUB_COMMON_TESTS_227C_H

#include <inttypes.h>

struct common_227c_test {
	uint16_t pre_unknown_8222;
	uint16_t pre_unknown_822c;
	int16_t pre_unknown_823a;
	uint16_t pre_gear_hi_5_lo_a;
	uint16_t pre_internal_brake_pedal;
	int16_t pre_high_precision_speed;
	int16_t pre_clipped_steering_candidate_2;
	int16_t post_unknown_8240;
	int32_t post_unknown_8268;
	int16_t post_sideways_pull;
	int16_t post_unknown_8248;
	uint16_t post_steering_multiplier;
	uint16_t post_skidding_volume;
};

extern int num_common_227c_tests;
extern struct common_227c_test common_227c_tests[];

void run_common_227c_tests();

#endif
