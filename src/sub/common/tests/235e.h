#ifndef __SUB_COMMON_TESTS_235E_H
#define __SUB_COMMON_TESTS_235E_H

#include <inttypes.h>

struct common_235e_test {
	int16_t pre_sideways_pull;
	uint16_t pre_unknown_8222;
	int16_t pre_unknown_823e;
	int16_t pre_unknown_827a;
	int16_t pre_clipped_steering;
	int16_t pre_clipped_steering_candidate_2;
	int16_t pre_high_precision_speed;
	uint16_t post_unknown_8222;
	int16_t post_unknown_827a;
	int16_t post_clipped_steering;
	int16_t post_clipped_steering_candidate_2;
};

extern int num_common_235e_tests;
extern struct common_235e_test common_235e_tests[];

void run_common_235e_tests();

#endif
