#ifndef __SUB_COMMON_TESTS_244C_H
#define __SUB_COMMON_TESTS_244C_H

#include <inttypes.h>

struct common_244c_test {
	uint16_t pre_steering_wheel_rotation;
	uint16_t pre_steering_wheel_rotation_candidate_2;
	int16_t pre_clipped_steering;
	int16_t pre_curve_at_player_forward_position;
	int16_t pre_high_precision_sideways_position;
	uint16_t pre_steering_multiplier;
	int32_t pre_current_speed_8_bit_candidate_3;
	int16_t post_steering_wheel_frame_delta;
	uint16_t post_steering_wheel_rotation_candidate_2;
	int16_t post_steering_or_car_orientation;	
	int16_t post_high_precision_sideways_position;
};

extern int num_common_244c_tests;
extern struct common_244c_test common_244c_tests[];

void run_common_244c_tests();

#endif
