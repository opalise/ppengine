#ifndef __SUB_COMMON_TESTS_2058_H
#define __SUB_COMMON_TESTS_2058_H

#include <inttypes.h>

struct common_2058_test {
	uint16_t pre_session_engine_started;
	uint16_t pre_waiting_for_coin;
	int16_t pre_target_revs_increase;
	uint16_t pre_gear_binary;
	int32_t pre_current_speed_8_bit_candidate_3;
	int16_t pre_actual_engine_revs;
	int16_t pre_target_engine_revs;
	int16_t pre_high_precision_speed;		
	int16_t post_target_actual_revs_relationship;
	int16_t post_target_engine_revs;
	int16_t post_actual_engine_revs;
	uint16_t post_gear_hi_5_lo_a;
};

extern int num_common_2058_tests;
extern struct common_2058_test common_2058_tests[];

void run_common_2058_tests();

#endif
