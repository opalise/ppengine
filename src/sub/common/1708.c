#include <stdio.h>
#include "sub/common/1708.h"
#include "sub/data/race_seconds_lookup.h"
#include "engine.h"

void common_1708() {
	int16_t lap_completion_events_counted_temp;
	uint16_t seconds_to_add;	
	if (new_lap_just_started) {
		lap_completion_events_counted_temp=lap_completion_events_counted-5;
		printf("1708: lap completion events counted = %d\n",lap_completion_events_counted);

		if (lap_completion_events_counted_temp>=0) {
			seconds_to_add=race_seconds_lookup[((((lap_completion_events_counted_temp+1)&3)*2)|((race_initial_timer_switch_2&7)<<3)|((race_initial_timer_switch_1&1)<<6))>>1];
			printf("seconds to add: %d\n",seconds_to_add);
			if ((seconds_to_add!=0) && (seconds_to_add<=100)) {
				seconds_remaining+=seconds_to_add;
				printf("number of cpu cars in use was %d\n",number_of_cpu_cars_in_use);
				number_of_cpu_cars_in_use++;
				if (number_of_cpu_cars_in_use>7) {
					number_of_cpu_cars_in_use=7;
				}
				if (((race_initial_timer_switch_1&1)==0) && (lap_completion_events_counted==5)) {
					number_of_cpu_cars_in_use++;
				}
				printf("number of cpu cars in use is now %d\n",number_of_cpu_cars_in_use);
				sample_player_1|=2;
				extended_play_frames_remaining=127;
				printf("extended play frames remaining is now %d\n",extended_play_frames_remaining);
				current_race_lap++;
			}
		}
	}
}


