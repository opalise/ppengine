#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include "tests.h"
#include "sub/common/tests/1844.h"
#include "sub/common/tests/148e.h"
#include "sub/common/tests/2058.h"
#include "sub/common/tests/212c.h"
#include "sub/common/tests/235e.h"
#include "sub/common/tests/2174.h"
#include "sub/common/tests/21b4.h"
#include "sub/common/tests/221a.h"
#include "sub/common/tests/227c.h"
#include "sub/common/tests/231e.h"
#include "sub/common/tests/24b0.h"
#include "sub/common/tests/244c.h"
#include "sub2/perframefunctions/tests/cpucarhiding.h"
#include "sub2/perframefunctions/tests/cpucarai.h"
#include "sub2/perframefunctions/tests/cpucarmovement.h"
#include "sub2/perframefunctions/tests/playercarmovement.h"

int main(void) {
	run_common_1844_tests();
	run_common_148e_tests();
	run_common_2058_tests();
	run_common_212c_tests();
	run_common_235e_tests();
	run_common_2174_tests();
	run_common_21b4_tests();
	run_common_221a_tests();
	run_common_227c_tests();
	run_common_231e_tests();
	run_common_24b0_tests();
	run_common_244c_tests();
	run_cpu_car_hiding_tests();
	run_cpu_car_ai_tests();
	run_cpu_car_movement_tests();
	run_player_car_movement_tests();
	return(0);
}


